from django import forms
from .models import Respuesta, Control, Pregunta, Dominio, Seccion

RESPUESTA = (
	('1','Si'),
	('0','No'),
	('2','Desconozco')
)

class RespuestaForm(forms.ModelForm):
	class Meta:
		model = Respuesta
		fields = ('respuesta',)

		widgets = {
			'respuesta': forms.RadioSelect(
				choices=RESPUESTA
			)
		}

class ControlForm(forms.ModelForm):
	class Meta:
		model = Control
		fields = ('nombre',)

		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class PreguntaForm(forms.ModelForm):
	class Meta:
		model = Pregunta
		fields = ('nombre',)

		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class DominioForm(forms.ModelForm):
	class Meta:
		model = Dominio
		fields = ('nombre',)

		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class SeccionForm(forms.ModelForm):
	class Meta:
		model = Seccion
		fields = ('nombre',)

		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}