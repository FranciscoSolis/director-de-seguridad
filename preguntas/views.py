from django.shortcuts import render, redirect
from .models import User, Dominio, Pregunta, Respuesta, Seccion, Control, DiasEncuestaEmpresa
from reportes.models import Intervalos, IntervalosAceptado
from .forms import RespuestaForm, ControlForm, PreguntaForm, DominioForm, SeccionForm
from django.http import Http404, HttpResponse
from usuarios.views import agregar_usuario, agregar_trabajador, agregar_supervisor
from usuarios.models import Logs
import datetime

# Funcion para obtener el id del usuario
def idusuario(request):	
	return request.user.id

def preguntas(request):
	userid=idusuario(request)
	permisos = User.objects.get(id = userid)
	if permisos.autorizacion.id_a == 3:
		return redirect('agregar_trabajador')
	elif permisos.autorizacion.id_a == 2:
		return redirect('agregar_usuario')
	elif permisos.autorizacion.id_a == 1:
		return redirect('/reportes')
	log = Logs(usuario = permisos)
	log.save()
	return redirect('responder')

def retroceder(request):
	userid=idusuario(request)
	falta = Respuesta.objects.filter(
		usuario_dueño = userid).count()
	if falta >= 1:
		Respuesta.objects.filter(
		usuario_dueño = userid, 
		pregunta = falta
		).delete()

	return redirect('responder')

def responder(request):
	template_name = 'responder.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	fecha_limite = DiasEncuestaEmpresa.objects.get(empresa = usuario.sucursal.empresa)
	hoy = datetime.datetime.now()
	if fecha_limite.fecha_fin.date() <= hoy.date():
		return redirect('respuesta')
	falta = Respuesta.objects.filter(
		usuario_dueño = userid).count()
	if falta == Pregunta.objects.all().count():
		return redirect('respuesta')
	form = RespuestaForm()
	if request.method == 'POST':
		form = RespuestaForm(request.POST)
		if form.is_valid():
			resp = form.save(commit=False)
			resp.usuario_dueño = User.objects.get(id = userid)
			resp.pregunta = Pregunta.objects.get(id_p = falta+1)
			resp.save()
			return redirect('responder')
	else:
		form = RespuestaForm()

	dominio_actual = Pregunta.objects.get(
		id_p = falta+1)
	cantidad_actual = Respuesta.objects.filter(
			usuario_dueño = userid, 
			pregunta__control__seccion__dominio = dominio_actual.control.seccion.dominio
			).count()
	data = {
		'form' : form,
		'pregunta' : Pregunta.objects.get(
			id_p = falta+1),
		'cantidad_dominio' : Pregunta.objects.filter(
			control__seccion__dominio = dominio_actual.control.seccion.dominio
			).count(),
		'cantidad_actual' : cantidad_actual+1
	}
	return render(request, template_name, data)

def respuesta(request):
	template_name = 'respuesta.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	fecha_limite = DiasEncuestaEmpresa.objects.get(empresa = usuario.sucursal.empresa)
	hoy = datetime.datetime.now()
	tiempo = 0
	if fecha_limite.fecha_fin.date() <= hoy.date():
		tiempo = 1
	data = {
		'respuestas' : Respuesta.objects.filter(
			usuario_dueño =  userid
		),
		'tiempo' : tiempo
	}
	return render(request, template_name, data)

def respuestas(request):
	userid=idusuario(request)
	template_name = 'respuestas.html'
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 3:
		respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal
			).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal
			).count()
	else:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).count()

	dominio = Dominio.objects.first()

	if cant_respuesta == 0:
		return  HttpResponse('<h1>Aún no existen respuestas en la encuesta</h1>')
	if usuario.autorizacion.id_a == 3:
		intervalo = Intervalos.objects.filter( empresa = usuario.sucursal.empresa).count()
	else:
		intervalo = Intervalos.objects.filter( empresa = usuario.empresa).count()
	if intervalo == 0:
		return  HttpResponse('<h1>Aún no se especifican intervalos en la empresa</h1>')

	si = 0
	no = 0
	cantidad = []
	cant = 0
	cantn = 0
	for x in respuesta:
		if dominio != x.pregunta.control.seccion.dominio:
			cant = si + no
			cant = (si*100)/cant
			cantn = 100-cant
			cantidad.append([dominio, cant, cantn])
			dominio = x.pregunta.control.seccion.dominio
			si = 0
			no = 0 
		if x.respuesta == 0:
			no=no+1
		if x.respuesta == 1:
			si=si+1
	cant = si + no
	cant = (si*100)/cant
	cantn = 100-cant
	cantidad.append([dominio, cant, cantn])
	if usuario.autorizacion.id_a == 3:
		data = {
			'respuesta' : cantidad,
			'dominio' : Dominio.objects.all(),
			'intervalo' :Intervalos.objects.get( empresa = usuario.sucursal.empresa)
			}
	else:
		data = {
			'respuesta' : cantidad,
			'dominio' : Dominio.objects.all(),
			'intervalo' :Intervalos.objects.get( empresa = usuario.empresa)
			}
	return render(request, template_name, data)

def respuestas_n3(request):
	template_name = 'respuestas_n3.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 3:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal
		).count()
	else:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).count()

	if cant_respuesta == 0:
		return  HttpResponse('<h1>Aún no existen respuestas en la encuesta</h1>')
	if usuario.autorizacion.id_a == 3:
		intervalo = Intervalos.objects.filter( empresa = usuario.sucursal.empresa).count()
	else:
		intervalo = Intervalos.objects.filter( empresa = usuario.empresa).count()
	if intervalo == 0:
		return  HttpResponse('<h1>Aún no se especifican intervalos en la empresa</h1>')
		
	dominio = Dominio.objects.first()
	seccion = Seccion.objects.first()

	# Dominio
	sid = 0
	nod = 0
	cantidadd = []
	cantd = 0
	cantnd = 0
	for x in respuesta:
		if dominio != x.pregunta.control.seccion.dominio:
			cantd = sid + nod
			cantd = (sid*100)/cantd
			cantnd = 100-cantd
			cantidadd.append([dominio, cantd, cantnd])
			dominio = x.pregunta.control.seccion.dominio
			sid = 0
			nod = 0 
		if x.respuesta == 0:
			nod=nod+1
		if x.respuesta == 1:
			sid=sid+1
	cantd = sid + nod
	cantd = (sid*100)/cantd
	cantnd = 100-cantd
	cantidadd.append([dominio, cantd, cantnd])

	# Seccion
	si = 0
	no = 0
	cantidad = []
	cant = 0
	cantn = 0
	for x in respuesta:
		if seccion != x.pregunta.control.seccion:
			cant = si + no
			cant = (si*100)/cant
			cantn = 100-cant
			cantidad.append([seccion, cant, cantn])
			seccion = x.pregunta.control.seccion
			si = 0
			no = 0 
		if x.respuesta == 0:
			no=no+1
		if x.respuesta == 1:
			si=si+1

	cant = si + no
	cant = (si*100)/cant
	cantn = 100-cant
	cantidad.append([seccion, cant, cantn])
	if usuario.autorizacion.id_a == 3:
		data = {
			'respuesta' : cantidad,
			'respuestad' : cantidadd,
			'dominio' : Dominio.objects.all(),
			'seccion' : Seccion.objects.all(),
			'intervalo' :Intervalos.objects.get( empresa = usuario.sucursal.empresa)
			}
	else:
		data = {
			'respuesta' : cantidad,
			'respuestad' : cantidadd,
			'dominio' : Dominio.objects.all(),
			'seccion' : Seccion.objects.all(),
			'intervalo' :Intervalos.objects.get( empresa = usuario.empresa)
			}

	return render(request, template_name, data)

def respuestas_n2(request):
	template_name = 'respuestas_n2.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 3:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal
		).count()
	else:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).count()

	if cant_respuesta == 0:
		return  HttpResponse('<h1>Aún no existen respuestas en la encuesta</h1>')
	if usuario.autorizacion.id_a == 3:
		intervalo = Intervalos.objects.filter( empresa = usuario.sucursal.empresa).count()
	else:
		intervalo = Intervalos.objects.filter( empresa = usuario.empresa).count()
	if intervalo == 0:
		return  HttpResponse('<h1>Aún no se especifican intervalos en la empresa</h1>')
		
	dominio = Dominio.objects.first()
	seccion = Seccion.objects.first()
	control = Control.objects.first()

	# Dominio
	sid = 0
	nod = 0
	cantidadd = []
	cantd = 0
	cantnd = 0
	for x in respuesta:
		if dominio != x.pregunta.control.seccion.dominio:
			cantd = sid + nod
			cantd = (sid*100)/cantd
			cantnd = 100-cantd
			cantidadd.append([dominio, cantd, cantnd])
			dominio = x.pregunta.control.seccion.dominio
			sid = 0
			nod = 0 
		if x.respuesta == 0:
			nod=nod+1
		if x.respuesta == 1:
			sid=sid+1
	cantd = sid + nod
	cantd = (sid*100)/cantd
	cantnd = 100-cantd
	cantidadd.append([dominio, cantd, cantnd])

	# Seccion
	si = 0
	no = 0
	cantidad = []
	cant = 0
	cantn = 0
	for x in respuesta:
		if seccion != x.pregunta.control.seccion:
			cant = si + no
			cant = (si*100)/cant
			cantn = 100-cant
			cantidad.append([seccion, cant, cantn])
			seccion = x.pregunta.control.seccion
			si = 0
			no = 0 
		if x.respuesta == 0:
			no=no+1
		if x.respuesta == 1:
			si=si+1

	cant = si + no
	cant = (si*100)/cant
	cantn = 100-cant
	cantidad.append([seccion, cant, cantn])

	# control
	sic = 0
	noc = 0
	cantidadc = []
	cantc = 0
	cantnc = 0
	for x in respuesta:
		if control != x.pregunta.control:
			cantc = sic + noc
			if cantc > 0:
				cantc = (sic*100)/cantc
				cantnc = 100-cantc
			else:
				cantc = 0
				cantnc = 0
			cantidadc.append([control, cantc, cantnc])
			control = x.pregunta.control
			sic = 0
			noc = 0 
		if x.respuesta == 0:
			noc=noc+1
		if x.respuesta == 1:
			sic=sic+1

	cantc = sic + noc
	if cantc > 0:
		cantc = (sic*100)/cantc
		cantnc = 100-cantc
	else:
		cantc = 0
		cantnc = 0
	cantidadc.append([control, cantc, cantnc])
	if usuario.autorizacion.id_a == 3:
		data = {
			'respuesta' : cantidad,
			'respuestad' : cantidadd,
			'respuestac' : cantidadc,
			'dominio' : Dominio.objects.all(),
			'seccion' : Seccion.objects.all(),
			'control' : Control.objects.all(),
			'intervalo' :Intervalos.objects.get( empresa = usuario.sucursal.empresa)
			}
	else:
		data = {
			'respuesta' : cantidad,
			'respuestad' : cantidadd,
			'respuestac' : cantidadc,
			'dominio' : Dominio.objects.all(),
			'seccion' : Seccion.objects.all(),
			'control' : Control.objects.all(),
			'intervalo' :Intervalos.objects.get( empresa = usuario.empresa)
			}	

	return render(request, template_name, data)

def grafico(request):
	template_name = 'grafico.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 3:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal
		).count()
	else:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).count()

	if cant_respuesta == 0:
		return  HttpResponse('<h1>Aún no existen respuestas en la encuesta</h1>')
	dominio = Dominio.objects.first()

	si = 0
	no = 0
	cantidad = []
	cant = 0
	cantn = 0
	for x in respuesta:
		if dominio != x.pregunta.control.seccion.dominio:
			cant = si + no
			cant = (si*100)/cant
			cantn = 100-cant
			cantidad.append([dominio.checklist, cant, cantn])
			dominio = x.pregunta.control.seccion.dominio
			si = 0
			no = 0 
		if x.respuesta == 0:
			no=no+1
		if x.respuesta == 1:
			si=si+1
	cant = si + no
	cant = (si*100)/cant
	cantn = 100-cant
	cantidad.append([dominio.checklist, cant, cantn])

	data = {
		'respuesta' : cantidad
		}

	return render(request, template_name, data)

def elegir_control(request):
	template_name = 'elegir_control.html'
	id_seccion = request.GET['id_seccion']
	data = {
	'control' : Control.objects.filter(
		seccion = id_seccion)
	}

	return render(request, template_name, data)


def editar_control(request):
	template_name = 'editar_control.html'
	userid = idusuario(request)
	id_control = request.GET['id_control']
	control = Control.objects.get(id_c = id_control)
	form = ControlForm(instance = control)
	if request.method == 'POST':
		form = ControlForm(request.POST, instance=control)
		if form.is_valid():
			nuevo = form.save(commit=False)
			nuevo.save()
			return redirect('elegir_dominio')
		else: 
			form = ControlForm(instance = control)
	data = {
	'form':form
	}

	return render(request, template_name, data)

def elegir_pregunta(request):
	template_name = 'elegir_pregunta.html'
	id_control = request.GET['id_control']
	data = {
	'pregunta' : Pregunta.objects.filter(
		control = id_control)
	}

	return render(request, template_name, data)

def editar_pregunta(request):
	template_name = 'editar_pregunta.html'
	userid = idusuario(request)
	id_pregunta = request.GET['id_pregunta']
	pregunta = Pregunta.objects.get(id_p = id_pregunta)
	form = PreguntaForm(instance = pregunta)
	if request.method == 'POST':
		form = PreguntaForm(request.POST, instance=pregunta)
		if form.is_valid():
			nuevo = form.save(commit=False)
			nuevo.save()
			return redirect('elegir_dominio')
		else: 
			form = PreguntaForm(instance = pregunta)
	data = {
	'form':form
	}

	return render(request, template_name, data)

def elegir_dominio(request):
	template_name = 'elegir_dominio.html'
	data = {
	'dominio' : Dominio.objects.all()
	}

	return render(request, template_name, data)

def editar_dominio(request):
	template_name = 'editar_dominio.html'
	userid = idusuario(request)
	id_dominio = request.GET['id_dominio']
	dominio = Dominio.objects.get(id_d = id_dominio)
	form = DominioForm(instance = dominio)
	if request.method == 'POST':
		form = DominioForm(request.POST, instance=dominio)
		if form.is_valid():
			nuevo = form.save(commit=False)
			nuevo.save()
			return redirect('elegir_dominio')
		else: 
			form = DominioForm(instance = dominio)
	data = {
	'form':form
	}

	return render(request, template_name, data)

def elegir_seccion(request):
	template_name = 'elegir_seccion.html'
	id_dominio = request.GET['id_dominio']
	data = {
	'seccion' : Seccion.objects.filter(
		dominio = id_dominio
		)
	}

	return render(request, template_name, data)


def editar_seccion(request):
	template_name = 'editar_seccion.html'
	userid = idusuario(request)
	id_seccion = request.GET['id_seccion']
	seccion = Seccion.objects.get(id_s = id_seccion)
	form = SeccionForm(instance = seccion)
	if request.method == 'POST':
		form = SeccionForm(request.POST, instance=seccion)
		if form.is_valid():
			nuevo = form.save(commit=False)
			nuevo.save()
			return redirect('elegir_dominio')
		else: 
			form = SeccionForm(instance = seccion)
	data = {
	'form':form
	}

	return render(request, template_name, data)


def editar_respuesta(request):
	template_name = 'editar_respuesta.html'
	data = {}
	id_respuesta = request.GET['id_respuesta']
	respuesta = Respuesta.objects.get(id_res = id_respuesta)
	form = RespuestaForm(instance = respuesta)
	if request.method == 'POST':
		form = RespuestaForm(request.POST, instance=respuesta)
		if form.is_valid():
			resp = form.save(commit=False)
			resp.save()
			return redirect('respuesta')
	else:
		form = RespuestaForm(instance = respuesta)

	data = {
		'form' : form,
		'respuesta' : respuesta
	}

	return render(request, template_name, data)

def grafico_desconozco(request):
	template_name = 'grafico_desconozco.html'
	seleccion = request.GET['seleccion']
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 3:
		if seleccion == "0":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal
			).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal
			).count()
		elif seleccion == "1":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				usuario_dueño__rol = 1
				).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				usuario_dueño__rol = 1
				).count()
		elif seleccion == "2":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				usuario_dueño__rol = 2
				).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				usuario_dueño__rol = 2
				).count()
		elif seleccion == "3":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				usuario_dueño__rol = 3
				).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				usuario_dueño__rol = 3
				).count()
	else:
		if seleccion == "0":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa
			).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa
			).count()
		elif seleccion == "1":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
				usuario_dueño__rol = 1
			).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
				usuario_dueño__rol = 1
			).count()
		elif seleccion == "2":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
				usuario_dueño__rol = 2
			).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
				usuario_dueño__rol = 2
			).count()
		elif seleccion == "3":
			respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
				usuario_dueño__rol = 3
			).order_by('pregunta')
			cant_respuesta = Respuesta.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
				usuario_dueño__rol = 3
			).count()
	if cant_respuesta == 0:
		return  HttpResponse('<h1>Aún no existen respuestas en la encuesta</h1>')
	dominio = Dominio.objects.first()

	si = 0
	no = 0
	desconozco = 0
	cantidad = []
	circular = []
	cant = 0
	cantn = 0
	cantd = 0
	for x in respuesta:
		if dominio != x.pregunta.control.seccion.dominio:
			cantidad.append([dominio.nombre, si, no, desconozco])
			dominio = x.pregunta.control.seccion.dominio
			si = 0
			no = 0 
			desconozco = 0
		if x.respuesta == 0:
			no=no+1
			cantn=cantn+1
		if x.respuesta == 1:
			si=si+1
			cant=cant+1
		if x.respuesta == 2:
			desconozco=desconozco+1
			cantd=cantd+1
	cantidad.append([dominio.nombre, si, no, desconozco])
	circular = [cant, cantn, cantd]


	data = {
		'respuesta' : cantidad,
		'circular' : circular
		}

	return render(request, template_name, data)

def grafico_directivo(request):
	template_name = 'grafico_directivo.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 3:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal,
			usuario_dueño__rol = 1
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal =  usuario.sucursal,
			usuario_dueño__rol = 1
		).count()
	else:
		respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa,
			usuario_dueño__rol = 1
		).order_by('pregunta')
		cant_respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa,
			usuario_dueño__rol = 1
		).count()

	if cant_respuesta == 0:
		return  HttpResponse('<h1>Aún no existen respuestas en la encuesta</h1>')
	dominio = Dominio.objects.first()

	si = 0
	no = 0
	desconozco = 0
	cantidad = []
	circular = []
	cant = 0
	cantn = 0
	cantd = 0
	for x in respuesta:
		if dominio != x.pregunta.control.seccion.dominio:
			cantidad.append([dominio.nombre, si, no, desconozco])
			dominio = x.pregunta.control.seccion.dominio
			si = 0
			no = 0 
			desconozco = 0
		if x.respuesta == 0:
			no=no+1
			cantn=cantn+1
		if x.respuesta == 1:
			si=si+1
			cant=cant+1
		if x.respuesta == 2:
			desconozco=desconozco+1
			cantd=cantd+1
	cantidad.append([dominio.nombre, si, no, desconozco])
	circular = [cant, cantn, cantd]


	data = {
		'respuesta' : cantidad,
		'circular' : circular
		}

	return render(request, template_name, data)

