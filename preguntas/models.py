from django.db import models
from usuarios.models import User, Empresa

class Dominio(models.Model):
	id_d = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	checklist = models.CharField(max_length=144)
	estandar = models.CharField(max_length=144)
	nombre = models.CharField(max_length=144)

	def __str__(self):
		return str(self.id_d)

class Seccion(models.Model):
	id_s = models.AutoField(primary_key = True, unique=True,null=False, blank = False)
	checklist = models.CharField(max_length=144)
	dominio = models.ForeignKey(Dominio, null=False, blank=False, on_delete=models.CASCADE)
	estandar = models.CharField(max_length=144)
	nombre = models.CharField(max_length=144)

	def __str__(self):
		return str(self.id_s)

class Control(models.Model):
	id_c = models.AutoField(primary_key = True, unique=True,null=False, blank = False)
	checklist = models.CharField(max_length=144)
	seccion = models.ForeignKey(Seccion, null=False, blank=False, on_delete=models.CASCADE)
	estandar = models.CharField(max_length=144)
	nombre = models.CharField(max_length=144)

	def __str__(self):
		return str(self.id_c)

class Pregunta(models.Model):
	id_p = models.IntegerField(primary_key = True, unique=True,null=False, blank = False)
	control = models.ForeignKey(Control, null=False, blank=False, on_delete=models.CASCADE)
	letra = models.CharField(max_length=2)
	nombre = models.TextField()

	def __str__(self):
		return str(self.id_p)

class Respuesta(models.Model):
	id_res = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	usuario_dueño = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	pregunta = models.ForeignKey(Pregunta, null=False, blank=False, on_delete=models.CASCADE)
	respuesta = models.IntegerField()

	def __str__(self):
		return str(self.id_res)

class DiasEncuestaEmpresa(models.Model):
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	fecha_inicio = models.DateTimeField(auto_now_add=True)
	fecha_fin = models.DateTimeField(auto_now_add=False)

	def __str__(self):
		return str(self.empresa)