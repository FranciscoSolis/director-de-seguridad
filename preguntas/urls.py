from django.urls import path
from preguntas import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', login_required(views.preguntas), name="preguntas"),
    path('responder/', login_required(views.responder), name="responder"),
    path('respuesta/', login_required(views.respuesta), name="respuesta"),
    path('respuestas/', login_required(views.respuestas), name="respuestas"),
    path('respuestas_n3/', login_required(views.respuestas_n3), name="respuestas_n3"),
    path('respuestas_n2/', login_required(views.respuestas_n2), name="respuestas_n2"),
    path('grafico/', login_required(views.grafico), name="grafico"),
    path('grafico_desconozco/', login_required(views.grafico_desconozco), name="grafico_desconozco"),
    path('elegir_control/', login_required(views.elegir_control), name="elegir_control"),
    path('elegir_control/editar_control/', login_required(views.editar_control), name="editar_control"),
    path('elegir_pregunta/', login_required(views.elegir_pregunta), name="elegir_pregunta"),
    path('elegir_pregunta/editar_pregunta/', login_required(views.editar_pregunta), name="editar_pregunta"),
    path('responder/retroceder', login_required(views.retroceder), name="retroceder"),
    path('elegir_dominio/', login_required(views.elegir_dominio), name="elegir_dominio"),
    path('elegir_dominio/editar_dominio/', login_required(views.editar_dominio), name="editar_dominio"),
    path('elegir_seccion/', login_required(views.elegir_seccion), name="elegir_seccion"),
    path('elegir_seccion/editar_seccion/', login_required(views.editar_seccion), name="editar_seccion"),
    path('editar_respuesta/', login_required(views.editar_respuesta), name="editar_respuesta")
]
