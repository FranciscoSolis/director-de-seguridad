from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import *

class DominioAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display = ('id_d','checklist','estandar','nombre')
	import_id_fields = ['checklist','estandar','nombre' ]

class SeccionAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display = ('id_s','checklist','dominio','estandar','nombre')

class ControlAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display = ('id_c','checklist','seccion','estandar','nombre')

class PreguntaAdmin(ImportExportModelAdmin, admin.ModelAdmin):
	list_display = ('id_p','control','letra','nombre')

admin.site.register(Dominio, DominioAdmin)
admin.site.register(Seccion, SeccionAdmin)
admin.site.register(Control, ControlAdmin)
admin.site.register(Pregunta, PreguntaAdmin)
admin.site.register(DiasEncuestaEmpresa)