from django.shortcuts import render, redirect
from .models import User, Reinicio, RespuestaReinicio, CumplimientoReinicio, RecoAutoEmpresaReinicio, DiagnosticoReinicio, IntervalosReinicio, IntervalosAceptadoReinicio
from preguntas.models import Respuesta, Dominio, DiasEncuestaEmpresa
from reportes.models import Cumplimiento, RecoAutoEmpresa, Diagnostico, Intervalos, IntervalosAceptado
import datetime

# Funcion para obtener el id del usuario
def idusuario(request):	
	return request.user.id

def reinicio(request):
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	reinicio = Reinicio.objects.filter(empresa = usuario.empresa).count()
	version = 0

	if reinicio == 0:
		version = 1
	elif reinicio>0:
		reinicio = Reinicio.objects.filter(empresa = usuario.empresa).order_by('-version').first()
		version = reinicio.version +1
	fecha = DiasEncuestaEmpresa.objects.get(empresa = usuario.empresa)
	dato = Reinicio(
		version = version,
		created_at = fecha.fecha_inicio,
		empresa = usuario.empresa
		)
	dato.save()
	reinicio = Reinicio.objects.get(empresa = usuario.empresa, version = version)
	for x in Respuesta.objects.filter(usuario_dueño__sucursal__empresa = usuario.empresa):
		dato = RespuestaReinicio(
			id_rer = x.id_res,
			usuario_dueño = x.usuario_dueño,
			pregunta = x.pregunta,
			respuesta = x.respuesta,
			reinicio = reinicio
			)
		dato.save()
	for x in Cumplimiento.objects.filter(usuario_delegado__sucursal__empresa = usuario.empresa):
		dato = CumplimientoReinicio(
			id_cumpr = x.id_cump,
			usuario_delegado = x.usuario_delegado,
			fecha_cumplido = x.fecha_cumplido,
			recomendacion = x.recomendacion,
			cumplimiento = x.cumplimiento,
			reinicio = reinicio
			)
		dato.save()
	for x in RecoAutoEmpresa.objects.filter(empresa = usuario.empresa):
		if x.cumplimiento:
			cumpli = x.cumplimiento.id_cump
			cumpli = CumplimientoReinicio.objects.get(id_cumpr = cumpli)
		else:
			cumpli=None
		dato = RecoAutoEmpresaReinicio(
			id_recaemr = x.id_recaem,
			empresa = x.empresa,
			recomendacion = x.recomendacion,
			cumplimiento = cumpli,
			created_at = x.created_at,
			vigencia = x.vigencia,
			reinicio = reinicio
			)
		dato.save()
	for x in Diagnostico.objects.filter(empresa = usuario.empresa):
		dato = DiagnosticoReinicio(
			id_dir = x.id_di,
			empresa = x.empresa,
			usuario_dueño = x.usuario_dueño,
			created_at = x.created_at,
			modified_at = x.modified_at,
			vigencia = x.vigencia,
			recomendacion = x.recomendacion,
			observaciones = x.observaciones,
			active = x.active,
			cumplimiento = x.cumplimiento,
			reinicio = reinicio
			)
		dato.save()
	for x in Intervalos.objects.filter(empresa = usuario.empresa):
		dato = IntervalosReinicio(
			id_inr = x.id_in,
			empresa = x.empresa,
			inter_alto = x.inter_alto,
			inter_medio = x.inter_medio,
			inter_bajo = x.inter_bajo,
			inter_nulo = x.inter_nulo,
			reinicio = reinicio
			)
		dato.save()
	for x in IntervalosAceptado.objects.filter(empresa = usuario.empresa):
		dato = IntervalosAceptadoReinicio(
			id_inacr = x.id_inac,
			empresa = x.empresa,
			inter_aceptado = x.inter_aceptado,
			reinicio = reinicio
			)
		dato.save()
		
	Respuesta.objects.filter(usuario_dueño__sucursal__empresa = usuario.empresa).delete()
	Cumplimiento.objects.filter(usuario_delegado__sucursal__empresa = usuario.empresa).delete()
	RecoAutoEmpresa.objects.filter(empresa = usuario.empresa).delete()
	Diagnostico.objects.filter(empresa = usuario.empresa).delete()
	Intervalos.objects.filter(empresa = usuario.empresa).delete()
	IntervalosAceptado.objects.filter(empresa = usuario.empresa).delete()
	DiasEncuestaEmpresa.objects.filter(empresa = usuario.empresa).delete()

	fecha_inicial = datetime.datetime.utcnow()
	fecha_final = fecha_inicial +datetime.timedelta(days=16)
	dias = DiasEncuestaEmpresa(
		empresa = usuario.empresa,
		fecha_inicio = fecha_inicial,
		fecha_fin = fecha_final)
	dias.save()
	return redirect('/reportes')


def datos_n1(request):
	template_name = 'datos_n1.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)

	if usuario.autorizacion.id_a == 3:
		respuesta = RespuestaReinicio.objects.filter(
				usuario_dueño__sucursal =  usuario.sucursal,
				reinicio__version = 1
			).order_by('pregunta')
	else:
		respuesta = RespuestaReinicio.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa,
				reinicio__version = 1
		).order_by('pregunta')

	dominio = Dominio.objects.first()
	versiones = Reinicio.objects.filter(empresa = usuario.empresa)
	si = 0
	no = 0
	cantidad = []
	cant = 0
	for y in versiones:
		for x in respuesta:
			if dominio != x.pregunta.control.seccion.dominio:
				cant = si + no
				if cant > 0:
					cant = (si*100)/cant
				else:
					cant = 0
				cantidad.append([dominio, cant, x.reinicio.version])
				dominio = x.pregunta.control.seccion.dominio
				si = 0
				no = 0
			if x.respuesta == 0:
				no=no+1
			if x.respuesta == 1:
				si=si+1
		cant = si + no
		if cant > 0:
			cant = (si*100)/cant
		else: 
			cant = 0
		cantidad.append([dominio, cant, x.reinicio.version])
		if usuario.autorizacion.id_a == 3:
			respuesta = RespuestaReinicio.objects.filter(
					usuario_dueño__sucursal =  usuario.sucursal,
					reinicio__version = y.version+1
				).order_by('pregunta')
		else:
			respuesta = RespuestaReinicio.objects.filter(
				usuario_dueño__sucursal__empresa =  usuario.empresa,
					reinicio__version = y.version+1
			).order_by('pregunta')
		dominio = Dominio.objects.first()
	print(cantidad)
	data = {
	'dominio' : Dominio.objects.all(),
	'reinicio' : versiones,
	'porcentaje' : cantidad
	}

	return render(request, template_name, data)