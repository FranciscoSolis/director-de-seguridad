from django.contrib import admin
from .models import *

admin.site.register(Reinicio)
admin.site.register(RespuestaReinicio)
admin.site.register(CumplimientoReinicio)
admin.site.register(RecoAutoEmpresaReinicio)
admin.site.register(DiagnosticoReinicio)
admin.site.register(IntervalosReinicio)
admin.site.register(IntervalosAceptadoReinicio)
