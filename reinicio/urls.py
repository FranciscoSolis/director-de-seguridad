from django.urls import path
from reinicio import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('reinicio/', login_required(views.reinicio), name="reinicio"),
    path('datos_n1/', login_required(views.datos_n1), name="datos_n1")
]
