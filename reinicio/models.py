from django.db import models
from usuarios.models import User, Empresa
from preguntas.models import Pregunta
from reportes.models import RecomendacionAuto

class Reinicio(models.Model):
	id_rei = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	created_at = models.DateTimeField(auto_now_add=False)
	version = models.IntegerField()
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_rei)

class RespuestaReinicio(models.Model):
	id_rer = models.IntegerField(primary_key = True, unique=True, null=False, blank = False)
	usuario_dueño = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	pregunta = models.ForeignKey(Pregunta, null=False, blank=False, on_delete=models.CASCADE)
	respuesta = models.IntegerField()
	reinicio = models.ForeignKey(Reinicio, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_rer)

class CumplimientoReinicio(models.Model):
	id_cumpr = models.IntegerField(primary_key = True, unique=True, null=False, blank = False)
	usuario_delegado = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	fecha_cumplido = models.DateTimeField(auto_now=True)
	recomendacion = models.ForeignKey(RecomendacionAuto, null=False, blank=False, on_delete=models.CASCADE)
	cumplimiento = models.BooleanField(default=False)
	reinicio = models.ForeignKey(Reinicio, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_cumpr)

class RecoAutoEmpresaReinicio(models.Model):
	id_recaemr = models.IntegerField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	recomendacion = models.ForeignKey(RecomendacionAuto, null=False, blank=False, on_delete=models.CASCADE)
	cumplimiento = models.ForeignKey(CumplimientoReinicio, null=True, blank=True, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True, null = True)
	vigencia = models.DateTimeField(auto_now_add=False, null = True)
	reinicio = models.ForeignKey(Reinicio, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_recaemr)

class DiagnosticoReinicio(models.Model):
	id_dir = models.IntegerField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	usuario_dueño = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	vigencia = models.DateTimeField('Vigencia de la medida',auto_now_add=False)
	recomendacion = models.TextField()
	observaciones = models.TextField(blank=True)
	active = models.BooleanField(default=True)
	cumplimiento = models.BooleanField(default=False)
	reinicio = models.ForeignKey(Reinicio, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_dir)

class IntervalosReinicio(models.Model):
	id_inr = models.IntegerField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	inter_alto = models.FloatField(default = 100)
	inter_medio = models.FloatField()
	inter_bajo = models.FloatField()
	inter_nulo = models.FloatField()
	reinicio = models.ForeignKey(Reinicio, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_inr)

class IntervalosAceptadoReinicio(models.Model):
	id_inacr = models.IntegerField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	inter_aceptado = models.IntegerField()
	reinicio = models.ForeignKey(Reinicio, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.id_inacr)