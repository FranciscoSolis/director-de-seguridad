from django.db import models
from usuarios.models import Empresa, User, Cargo
from preguntas.models import Pregunta, Dominio

class Intervalos(models.Model):
	id_in = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	inter_alto = models.FloatField(default = 100)
	inter_medio = models.FloatField()
	inter_bajo = models.FloatField()
	inter_nulo = models.FloatField()

	def __str__(self):
		return str(self.id_in)

class Diagnostico(models.Model):
	id_di = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	usuario_dueño = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now=True)
	vigencia = models.DateTimeField('Vigencia de la medida',auto_now_add=False)
	recomendacion = models.TextField()
	observaciones = models.TextField(blank=True)
	active = models.BooleanField(default=True)
	cumplimiento = models.BooleanField(default=False)

	def __str__(self):
		return str(self.empresa)

class RecomendacionAuto(models.Model):
	id_rec = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	pregunta = models.ForeignKey(Pregunta, null=False, blank=False, on_delete=models.CASCADE)
	cargo_recomendado = models.ForeignKey(Cargo, null=False, blank=False, on_delete=models.CASCADE)
	vigencia = models.IntegerField('cantidad de días')
	recomendacion = models.TextField()
	observaciones = models.TextField(blank=True)
	active = models.BooleanField(default=True)

	def __str__(self):
		return str(self.id_rec)

class Cumplimiento(models.Model):
	id_cump = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	usuario_delegado = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	fecha_cumplido = models.DateTimeField(auto_now=True)
	recomendacion = models.ForeignKey(RecomendacionAuto, null=False, blank=False, on_delete=models.CASCADE)
	cumplimiento = models.BooleanField(default=False)

	def __str__(self):
		return str(self.id_cump)

class IntervalosAceptado(models.Model):
	id_inac = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	inter_aceptado = models.IntegerField()

	def __str__(self):
		return str(self.id_inac)

class RecoAutoEmpresa(models.Model):
	id_recaem = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	recomendacion = models.ForeignKey(RecomendacionAuto, null=False, blank=False, on_delete=models.CASCADE)
	cumplimiento = models.ForeignKey(Cumplimiento, null=True, blank=True, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True, null = True)
	inicio_vigencia = models.DateTimeField(auto_now_add=False, null = True)
	vigencia = models.DateTimeField(auto_now_add=False, null = True)

	def __str__(self):
		return str(self.id_recaem)

class AspectosLegales(models.Model):
	id_asl = models.AutoField(primary_key = True, unique=True, null=False, blank = False)
	descripcion = models.CharField(max_length=144)
	tipo_norma = models.CharField(max_length=144)
	fecha_publicacion = models.DateTimeField(auto_now_add=False)
	fecha_promulgacion = models.DateTimeField(auto_now_add=False)
	organismo = models.CharField(max_length=144)
	titulo = models.TextField()
	tipo_version = models.CharField(max_length=144)
	inicio_vigencia = models.DateTimeField(auto_now_add=False)
	id_norma = models.IntegerField()
	texto_derogado = models.CharField(max_length=144, blank = True, null=True)
	ultima_modificacion = models.DateTimeField(blank = True, auto_now_add=False, null=True)
	url = models.TextField()
	active = models.BooleanField(default=True)
	version = models.IntegerField(default=1)
	version_anterior = models.ForeignKey('AspectosLegales', null=True, blank=True, on_delete=models.CASCADE)
	eliminado = models.BooleanField(default=False)

	def __str__(self):
		return str(self.tipo_norma)

class DominioLeyes(models.Model):
	dominio = models.ForeignKey(Dominio, null=False, blank=False, on_delete=models.CASCADE)
	aspecto_legal = models.ForeignKey(AspectosLegales, null=False, blank=False, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.dominio)


class RecomendacionesLogs(models.Model):
	cumplimiento = models.ForeignKey(Cumplimiento, null=True, blank=True, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	estado = models.BooleanField(default=True)

	def __str__(self):
		return str(self.cumplimiento)
