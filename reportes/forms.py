from django import forms
from .models import Intervalos, Diagnostico, IntervalosAceptado, RecomendacionAuto, Cumplimiento, User, AspectosLegales, DominioLeyes

RESPUESTA = (
	('10','10%'),
	('20','20%'),
	('30','30%'),
	('40','40%'),
	('50','50%'),
	('60','60%'),
	('70','70%'),
	('80','80%'),
	('90','90%'),
)

class IntervalosForm(forms.ModelForm):
	class Meta:
		model = Intervalos
		fields = ('inter_medio','inter_bajo','inter_nulo')

		widgets = {
			'inter_medio': forms.Select(
				choices=RESPUESTA
			),
			'inter_bajo': forms.Select(
				choices=RESPUESTA
			),
			'inter_nulo': forms.Select(
				choices=RESPUESTA
			),
		}

class DiagnosticoForm(forms.ModelForm):
	class Meta:
		model = Diagnostico
		fields = ('vigencia','observaciones','recomendacion')

		widgets = {
			'vigencia': forms.DateInput(
				attrs = {
					'class':'form-control',
					'placeholder':'AAAA-MM-DD'
				}
			),
			'observaciones': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			),
			'recomendacion': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			)
		}

class IntervalosAcepForm(forms.ModelForm):
	class Meta:
		model = IntervalosAceptado
		fields = ('inter_aceptado',)

		widgets = {
			'inter_aceptado': forms.Select(
				choices=RESPUESTA
			)
		}

class RecomendacionAutoForm(forms.ModelForm):
	class Meta:
		model = RecomendacionAuto
		fields = ('cargo_recomendado', 'vigencia', 'recomendacion', 'observaciones')

		widgets = {
			'vigencia': forms.NumberInput(
				attrs = {
					'class':'form-control'
				}
			),'recomendacion': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			),'observaciones': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			)
		}

class CumplimientoForm(forms.ModelForm):
	class Meta:
		model = Cumplimiento
		fields = ('usuario_delegado',)
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		super().__init__(*args, **kwargs)
		self.fields['usuario_delegado'].queryset =  User.objects.filter(sucursal__empresa = user.empresa, autorizacion = 4)

class CumplimientoSucursalForm(forms.ModelForm):
	class Meta:
		model = Cumplimiento
		fields = ('usuario_delegado',)
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		super().__init__(*args, **kwargs)
		self.fields['usuario_delegado'].queryset =  User.objects.filter(sucursal__empresa = user.sucursal.empresa, autorizacion = 4)

class AspectosLegalesForm(forms.ModelForm):
	class Meta:
		model = AspectosLegales
		fields = ('descripcion','tipo_norma','fecha_publicacion', 'fecha_promulgacion','organismo','titulo',
			'tipo_version','inicio_vigencia','id_norma','texto_derogado','ultima_modificacion','url', 'active')

		widgets = {
			'descripcion': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),'tipo_norma': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),'fecha_publicacion': forms.DateInput(
				format='%Y-%m-%d',
				attrs = {
					'class':'form-control',
					'placeholder':'AAAA-MM-DD'
				}
			),'fecha_promulgacion': forms.DateInput(
				format='%Y-%m-%d',
				attrs = {
					'class':'form-control',
					'placeholder':'AAAA-MM-DD'
				}
			),'organismo': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),'titulo': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			),'tipo_version': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),'inicio_vigencia': forms.DateInput(
				format='%Y-%m-%d',
				attrs = {
					'class':'form-control',
					'placeholder':'AAAA-MM-DD'
				}
			),'id_norma': forms.NumberInput(
				attrs = {
					'class':'form-control'
				}
			),'texto_derogado': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),'ultima_modificacion': forms.DateInput(
				format='%Y-%m-%d',
				attrs = {
					'class':'form-control',
					'placeholder':'AAAA-MM-DD'
				}
			),'url': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			),'active': forms.CheckboxInput(
				attrs = {
				}
			)
		}

class DominioLeyesForm(forms.ModelForm):
	class Meta:
		model = DominioLeyes
		fields = ('aspecto_legal',)
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['aspecto_legal'].queryset =  AspectosLegales.objects.filter(active = True)