from django.contrib import admin
from .models import *

admin.site.register(Intervalos)
admin.site.register(Diagnostico)
admin.site.register(RecomendacionAuto)
admin.site.register(Cumplimiento)
admin.site.register(IntervalosAceptado)
admin.site.register(RecoAutoEmpresa)
admin.site.register(AspectosLegales)
admin.site.register(DominioLeyes)
admin.site.register(RecomendacionesLogs)