from django.shortcuts import render, redirect
from django.http import Http404, HttpResponse
from .models import RecomendacionesLogs, Intervalos, Diagnostico, IntervalosAceptado, RecomendacionAuto, RecoAutoEmpresa, Cumplimiento, AspectosLegales, DominioLeyes
from usuarios.models import User, Empresa
from .forms import IntervalosForm, DiagnosticoForm, IntervalosAcepForm, RecomendacionAutoForm, CumplimientoForm, CumplimientoSucursalForm, AspectosLegalesForm, DominioLeyesForm
from usuarios.forms import UsuarioForm
from preguntas.views import respuestas, elegir_dominio
from preguntas.models import Respuesta, Pregunta, Dominio
import datetime

# Funcion para obtener el id del usuario
def idusuario(request):	
	return request.user.id

def reporte(request):
	template_name = 'reporte.html'
	data = {
	'empresas' : Empresa.objects.all()
	}

	return render(request, template_name, data)

def intervalos(request):
	template_name = 'intervalos.html'
	form = IntervalosForm()
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	if request.method == 'POST':
		form = IntervalosForm(request.POST)
		if form.is_valid():
			inte = form.save(commit=False)
			inte.empresa = Empresa.objects.get(id_e = usuario.empresa.id_e)
			inte.inter_alto = 100
			if inte.inter_nulo > inte.inter_bajo or inte.inter_nulo > inte.inter_medio or inte.inter_bajo > inte.inter_medio:
			 	return HttpResponse('<h1>Los intervalos deben ser de menos a mayor <br> Ej: 0 - 20 - 40 - 60 - 100</h1>')
			else:
				anterior = Intervalos.objects.filter(empresa = usuario.empresa.id_e).count()
				if anterior == 0:
					inte.save()
					return redirect('intervalos')
				else:
					Intervalos.objects.filter(empresa = usuario.empresa.id_e).delete()
					inte.save()
					return redirect('intervalos')

	else:
		form = IntervalosForm()
	data = {
	'form' : form,
	}

	return render(request, template_name, data)

def informe(request):
	if request.method == 'POST':
		id_empresa = request.POST['id_empresa']
	userid=idusuario(request)
	user = User.objects.get(id = userid)
	user.empresa = Empresa.objects.get(id_e = id_empresa)
	user.save()
	return redirect('respuestas')

def diagnostico(request):
	template_name = 'diagnostico.html'
	userid = idusuario(request)
	form = DiagnosticoForm()
	usuario = User.objects.get(id = userid)
	if request.method == 'POST':
		form = DiagnosticoForm(request.POST)
		if form.is_valid():
			diagnos = form.save(commit=False)
			diagnos.empresa = Empresa.objects.get(id_e = usuario.empresa.id_e)
			diagnos.usuario_dueño = User.objects.get( id = userid)
			diagnos.save()
			return redirect('respuestas')
		else:
			form = DiagnosticoForm()

	data = {
	'form' : form,
	}

	return render(request, template_name, data)

def diagnosticos(request):
	template_name = 'diagnosticos.html'
	userid = idusuario(request)
	permisos = User.objects.get(id = userid)
	data = {}
	if permisos.autorizacion.id_a == 1:
		data['diagnosticos'] = Diagnostico.objects.filter(
		usuario_dueño = userid,
		active = True
		).order_by('-created_at')
	elif permisos.autorizacion.id_a == 2:
		data['diagnosticos'] = Diagnostico.objects.filter(
		empresa = permisos.empresa.id_e,
		active = True
		).order_by('-created_at')
	return render(request, template_name, data)

def intervalo_aceptado(request):
	template_name = 'intervalo_aceptado.html'
	form = IntervalosAcepForm()
	userid = idusuario(request)
	usuario = User.objects.get(id = userid)
	interval = IntervalosAceptado.objects.filter(empresa = usuario.empresa.id_e).count()
	if request.method == 'POST':
		form = IntervalosAcepForm(request.POST)
		if form.is_valid():
			inter = form.save(commit=False)
			inter.empresa = Empresa.objects.get(id_e = usuario.empresa.id_e)
			if interval > 0:
				IntervalosAceptado.objects.filter(
					empresa = usuario.empresa.id_e
					).delete()
			inter.save()
			return redirect('intervalos')
		else:
			form = IntervalosAcepForm()
	data = {
	'form' : form
	}

	return render(request, template_name, data)

def editar_recomendacion(request):
	template_name = 'editar_recomendacion.html'
	userid = idusuario(request)
	id_pregunta = request.GET['id_pregunta']
	recomendacion = RecomendacionAuto.objects.get(pregunta = id_pregunta)
	form = RecomendacionAutoForm(instance = recomendacion)
	if request.method == 'POST':
		form = RecomendacionAutoForm(request.POST, instance=recomendacion)
		if form.is_valid():
			nuevo = form.save(commit=False)
			nuevo.save()
			return redirect('elegir_dominio')
		else: 
			form = RecomendacionAutoForm(instance = recomendacion)
	data = {
	'form':form
	}

	return render(request, template_name, data)

def enviar_recomendaciones_automaticas(request):
	userid = idusuario(request)
	usuario = User.objects.get(id = userid)
	empresa = Empresa.objects.get( id_e = usuario.empresa.id_e)
	intervalo = IntervalosAceptado.objects.get(empresa = usuario.empresa.id_e)
	respuesta = Respuesta.objects.filter(
			usuario_dueño__sucursal__empresa =  usuario.empresa
		).order_by('pregunta')
	pregunta = Pregunta.objects.first()
	cantidad_auto = RecoAutoEmpresa.objects.filter(empresa = empresa).count()
	if cantidad_auto > 0:
		RecoAutoEmpresa.objects.filter(empresa = empresa).delete()
		Cumplimiento.objects.filter(usuario_delegado__sucursal__empresa = empresa).delete()
	dominio = Dominio.objects.first()
	si = 0
	no = 0
	cant = 0
	dia_inicial = datetime.datetime.utcnow()
	suma_dias = 0
	for x in respuesta:
		if pregunta != x.pregunta:
			cant = si + no
			if cant != 0:
				cant = (si*100)/cant
			else:
				cant=0
			if cant<=intervalo.inter_aceptado:
				recom = RecomendacionAuto.objects.filter(pregunta = x.pregunta).first()
				r = RecoAutoEmpresa(
						empresa = empresa,
						recomendacion = recom,
						inicio_vigencia = dia_inicial,
						vigencia = dia_inicial+datetime.timedelta(days=recom.vigencia)
						)
				r.save()
				suma_dias = suma_dias + recom.vigencia
				if dominio != x.pregunta.control.seccion.dominio:
					dia_inicial = dia_inicial+datetime.timedelta(days=suma_dias)
					dominio = x.pregunta.control.seccion.dominio
					suma_dias = 0
			pregunta = x.pregunta
			si = 0
			no = 0
		if x.respuesta == 0:
			no=no+1
		if x.respuesta == 1:
			si=si+1
	cant = si + no
	cant = (si*100)/cant
	if cant<=intervalo.inter_aceptado:
		recom = RecomendacionAuto.objects.filter(pregunta = x.pregunta.id_p).first()
		r = RecoAutoEmpresa(
			empresa = empresa,
			recomendacion = recom,
			vigencia = datetime.datetime.utcnow()+datetime.timedelta(days=recom.vigencia)
			)
		r.save()

	return redirect('respuestas')

def reportes_auto(request):
	template_name = 'reportes_auto.html'
	userid = idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 2:
		recomau = RecoAutoEmpresa.objects.filter(empresa = usuario.empresa.id_e).order_by('recomendacion__vigencia')

	elif usuario.autorizacion.id_a == 3:
		recomau = RecoAutoEmpresa.objects.filter(empresa = usuario.sucursal.empresa.id_e).order_by('recomendacion__vigencia')

	data = {
	'recomau' : recomau,
	'aspectoslegales' : DominioLeyes.objects.all()
	}
	return render(request, template_name, data)

def establecer_encargado(request):
	template_name = 'establecer_encargado.html'
	userid = idusuario(request)
	usuario = User.objects.get(id = userid)
	id_recomendacion = request.GET['id_recomendacion']
	recomen = RecomendacionAuto.objects.get(id_rec = id_recomendacion)
	if usuario.autorizacion.id_a == 2:
		form = CumplimientoForm(user=request.user)
	elif usuario.autorizacion.id_a == 3:
		form = CumplimientoSucursalForm(user=request.user)
	if request.method == 'POST':
		if usuario.autorizacion.id_a == 2:
			form = CumplimientoForm(request.POST, user=request.user)
		elif usuario.autorizacion.id_a == 3:
			form = CumplimientoSucursalForm(request.POST, user=request.user)
		if form.is_valid():
			nuevo = form.save(commit=False)
			nuevo.recomendacion = recomen
			nuevo.save()
			if usuario.autorizacion.id_a == 2:
				rae = RecoAutoEmpresa.objects.get(
					empresa = usuario.empresa.id_e,
					recomendacion = recomen
					)
			elif usuario.autorizacion.id_a == 3:
				rae = RecoAutoEmpresa.objects.get(
					empresa = usuario.sucursal.empresa.id_e,
					recomendacion = recomen
					)
			id_cumplimiento = Cumplimiento.objects.get(
				usuario_delegado = nuevo.usuario_delegado,
				recomendacion = recomen
				)
			rae.cumplimiento = id_cumplimiento
			rae.save()
			return redirect('reportes_auto')
		else: 
			if usuario.autorizacion.id_a == 2:
				form = CumplimientoForm(user=request.user)
			elif usuario.autorizacion.id_a == 3:
				form = CumplimientoSucursalForm(user=request.user)
	data = {
	'form':form
	}
	return render(request, template_name, data)

def cumplido(request):
	template_name = 'cumplido.html'
	userid = idusuario(request)
	data = {
	'reportes' : RecoAutoEmpresa.objects.filter(cumplimiento__usuario_delegado = userid),
	'aspectoslegales' : DominioLeyes.objects.all()
	}

	return render(request, template_name, data)

def cumplir(request):
	userid = idusuario(request)
	confirmacion = request.POST['confirmacion']
	id_recomendacion = request.POST['id_recomendacion']
	c = Cumplimiento.objects.get(
		usuario_delegado = userid,
		recomendacion = id_recomendacion
		)
	if confirmacion == "1":
		c.cumplimiento = True
	else:
		c.cumplimiento = False
	c.save()

	rc = RecomendacionesLogs(
		cumplimiento = c,
		estado = c.cumplimiento
		)
	rc.save()
	return redirect('cumplido')

def aspectoslegales(request):
	template_name = 'aspectoslegales.html'

	data = {
	'aspectoslegales' : AspectosLegales.objects.filter(
		active = True)
	}

	return render(request, template_name, data)

def ver_aspectos_legales(request):
	template_name = 'ver_aspectos_legales.html'
	id_aspectoslegales = request.GET['id_aspectoslegales']

	data = {
	'aspectoslegales' : AspectosLegales.objects.get(id_asl = id_aspectoslegales)
	}

	return render(request, template_name, data)

def agregar_aspectos_legales(request):
	template_name = 'agregar_aspectos_legales.html'
	form = AspectosLegalesForm()
	if request.method == 'POST':
		form = AspectosLegalesForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('aspectoslegales')
		else:
			form = AspectosLegalesForm()
	data = {
	'form' : form
	}

	return render(request, template_name, data)

def editar_aspectos_legales(request):
	template_name = 'editar_aspectos_legales.html'
	id_aspectoslegales = request.GET['id_aspectoslegales']
	id_aspleg = AspectosLegales.objects.get(id_asl = id_aspectoslegales)
	form = AspectosLegalesForm(instance = id_aspleg)
	if request.method == 'POST':
		id_aspleg.active = False
		form = AspectosLegalesForm(request.POST)
		if form.is_valid():
			asp = form.save(commit=False)
			asp.version = id_aspleg.version+1
			asp.active = True
			asp.version_anterior = id_aspleg
			asp.save()
			id_aspleg.save()
			return redirect('aspectoslegales')
		else:
			form = AspectosLegalesForm(instance = id_aspleg)
	data = {
	'form' : form
	}

	return render(request, template_name, data)

def eliminar_aspectos_legales(request):
	id_aspectoslegales = request.GET['id_aspectoslegales']
	id_aspleg = AspectosLegales.objects.get(id_asl = id_aspectoslegales)
	id_aspleg.active = False
	id_aspleg.eliminado = True
	id_aspleg.save()
	return redirect('aspectoslegales')

def restaurar_aspectos_legales(request):
	template_name = 'restaurar_aspectos_legales.html'

	data = {
	'aspectoslegales' : AspectosLegales.objects.filter(
		active = False,
		eliminado = True)
	}

	return render(request, template_name, data)

def restaurar_seleccionado(request):
	id_aspectoslegales = request.GET['id_aspectoslegales']
	id_aspleg = AspectosLegales.objects.get(id_asl = id_aspectoslegales)
	id_aspleg.active = True
	id_aspleg.eliminado = False
	id_aspleg.save()
	return redirect('aspectoslegales')

def ver_leyes_dominio(request):
	template_name = 'ver_leyes_dominio.html'
	id_dominio = request.GET['id_dominio']
	id_dom = Dominio.objects.get(id_d = id_dominio)
	data = {
	'dominio' : DominioLeyes.objects.filter(
		dominio = id_dom),
	'id_dominio': id_dominio
	}

	return render(request, template_name, data)

def agregar_leyes_dominio(request):
	template_name = 'agregar_leyes_dominio.html'
	id_dominio = request.GET['id_dominio']
	id_dom = Dominio.objects.get(id_d = id_dominio)
	form = DominioLeyesForm()
	if request.method == 'POST':
		form = DominioLeyesForm(request.POST)
		if form.is_valid():
			guard = form.save(commit=False)
			revision = DominioLeyes.objects.filter(
						dominio = id_dom,
						aspecto_legal = guard.aspecto_legal
						).count()
			if revision==0:
				guard.dominio = id_dom
				guard.save()
			return redirect('elegir_dominio')
		else:
			form = DominioLeyesForm()
	data = {
	'form' : form
	}

	return render(request, template_name, data)

def eliminar_leyes_dominio(request):
	id_dominio = request.GET['id_dominio']
	id_aspectoslegales = request.GET['id_aspectoslegales']
	id_dom = Dominio.objects.get(id_d = id_dominio)
	id_asplegal = AspectosLegales.objects.get(id_asl = id_aspectoslegales)
	id_aspleg = DominioLeyes.objects.get(
		dominio = id_dom,
		aspecto_legal = id_asplegal)
	id_aspleg.delete()
	return redirect('elegir_dominio')

def grafico_recomendaciones(request):
	template_name = 'grafico_recomendaciones.html'
	userid = idusuario(request)
	usuario = User.objects.get(id = userid)
	if usuario.autorizacion.id_a == 2:
		recomau = RecoAutoEmpresa.objects.filter(empresa = usuario.empresa.id_e).order_by('recomendacion__pregunta')
	elif usuario.autorizacion.id_a == 3:
		recomau = RecoAutoEmpresa.objects.filter(empresa = usuario.sucursal.empresa.id_e).order_by('recomendacion__pregunta')
	dominio = Dominio.objects.first()
	cantidad = []
	i = 0
	for x in recomau:
		if dominio != x.recomendacion.pregunta.control.seccion.dominio:
			cantidad.append([dominio.nombre, i])
			dominio = x.recomendacion.pregunta.control.seccion.dominio
			i = 1
		else:
			i = i+1
	cantidad.append([dominio.nombre, i])
	data = {
	'cantidad' : cantidad
	}
	return render(request, template_name, data)

def version_anterior(request):
	id_aspectoslegales = request.GET['id_aspectoslegales']
	id_aspleg = AspectosLegales.objects.get(id_asl = id_aspectoslegales)
	id_anterior = id_aspleg.version_anterior
	id_anterior.active  = True
	id_anterior.save()
	id_aspleg.delete()
	return redirect('aspectoslegales')

def grafico_gantt(request):
	template_name = 'grafico_gantt.html'
	userid = idusuario(request)
	usuario = User.objects.get(id = userid)
	final = []
	dominio = Dominio.objects.first()
	fecha_inicial = datetime.datetime.utcnow()
	recomendaciones = RecoAutoEmpresa.objects.filter(
		empresa = usuario.empresa)
	for x in recomendaciones:
		if dominio != x.recomendacion.pregunta.control.seccion.dominio:
			final.append([dominio, int(fecha_inicial.strftime('%Y')), int(fecha_inicial.strftime('%m')), int(fecha_inicial.strftime('%d')), int(x.vigencia.strftime('%Y')), int(x.vigencia.strftime('%m')), int(x.vigencia.strftime('%d'))])
			dominio = x.recomendacion.pregunta.control.seccion.dominio
			fecha_inicial = x.inicio_vigencia + datetime.timedelta(days=2)
	
	final.append([dominio, int(fecha_inicial.strftime('%Y')), int(fecha_inicial.strftime('%m')), int(fecha_inicial.strftime('%d')), int(x.vigencia.strftime('%Y')), int(x.vigencia.strftime('%m')), int(x.vigencia.strftime('%d'))])
	data = {
		'recomendaciones' : final
	}
	return render(request, template_name, data)
