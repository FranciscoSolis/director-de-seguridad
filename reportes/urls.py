from django.urls import path
from reportes import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', login_required(views.reporte), name="reporte"),
    path('intervalos/', login_required(views.intervalos), name="intervalos"),
    path('informe/', login_required(views.informe), name="informe"),
    path('gap/', login_required(views.diagnostico), name="diagnostico"),
    path('diagnosticos/', login_required(views.diagnosticos), name="diagnosticos"),
    path('intervalo_aceptado/', login_required(views.intervalo_aceptado), name="intervalo_aceptado"),
    path('editar_recomendacion/', login_required(views.editar_recomendacion), name="editar_recomendacion"),
    path('enviar_recomendaciones_automaticas/', login_required(views.enviar_recomendaciones_automaticas), name="enviar_recomendaciones_automaticas"),
    path('reportes_auto/', login_required(views.reportes_auto), name="reportes_auto"),
    path('reportes_auto/establecer_encargado/', login_required(views.establecer_encargado), name="establecer_encargado"),
    path('cumplido/', login_required(views.cumplido), name="cumplido"),
    path('cumplido/cumplir/', login_required(views.cumplir), name="cumplir"),
    path('aspectoslegales/', login_required(views.aspectoslegales), name="aspectoslegales"),
    path('aspectoslegales/ver_aspectos_legales/', login_required(views.ver_aspectos_legales), name="ver_aspectos_legales"),
    path('aspectoslegales/agregar_aspectos_legales/', login_required(views.agregar_aspectos_legales), name="agregar_aspectos_legales"),
    path('aspectoslegales/ver_aspectos_legales/editar_aspectos_legales/', login_required(views.editar_aspectos_legales), name="editar_aspectos_legales"),
    path('aspectoslegales/ver_aspectos_legales/version_anterior/', login_required(views.version_anterior), name="version_anterior"),
    path('aspectoslegales/ver_aspectos_legales/eliminar_aspectos_legales/', login_required(views.eliminar_aspectos_legales), name="eliminar_aspectos_legales"),
    path('aspectoslegales/restaurar_aspectos_legales/', login_required(views.restaurar_aspectos_legales), name="restaurar_aspectos_legales"),
    path('aspectoslegales/restaurar_aspectos_legales/restaurar_seleccionado/', login_required(views.restaurar_seleccionado), name="restaurar_seleccionado"),
    path('ver_leyes_dominio/', login_required(views.ver_leyes_dominio), name="ver_leyes_dominio"),
    path('ver_leyes_dominio/agregar_leyes_dominio/', login_required(views.agregar_leyes_dominio), name="agregar_leyes_dominio"),
    path('ver_leyes_dominio/eliminar_leyes_dominio/', login_required(views.eliminar_leyes_dominio), name="eliminar_leyes_dominio"),
    path('grafico_recomendaciones/', login_required(views.grafico_recomendaciones), name="grafico_recomendaciones"),
    path('grafico_gantt/', login_required(views.grafico_gantt), name="grafico_gantt")
]
