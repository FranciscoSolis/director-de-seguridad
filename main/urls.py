from django.contrib import admin, auth
from django.urls import path, include
from django.contrib.auth.views import logout_then_login
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('base.urls')),
    path('preguntas/', include('preguntas.urls')),
    path('usuarios/', include('usuarios.urls')),
    path('reportes/', include('reportes.urls')),
    path('antiguo/', include('reinicio.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/login/', include('base.urls')),
    path('logout/', logout_then_login)
]
