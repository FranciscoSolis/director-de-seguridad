from django.db import models
from django.contrib.auth.models import AbstractUser

class Empresa(models.Model):
	id_e = models.AutoField(primary_key = True, null=False, blank = False)
	nombre = models.CharField(max_length=144)
	giro = models.CharField(max_length=144)
	rut = models.CharField( max_length=12)
	direccion = models.CharField(max_length=144)
	telefono = models.IntegerField()
	email = models.EmailField( max_length=254)

	def __str__(self):
		return str(self.nombre)

class Sucursal(models.Model):
	id_su = models.AutoField(primary_key = True, null=False, blank = False)
	empresa = models.ForeignKey(Empresa, null=False, blank=False, on_delete=models.CASCADE)
	nombre = models.CharField(max_length=144)

	def __str__(self):
		return str(self.nombre)

class Autorizacion(models.Model):
	id_a = models.AutoField(primary_key = True, null=False, blank = False)
	nombre = models.CharField(max_length=144)
	# Permisos 1 = Administrador General
	# Permisos 2 = Administrador de empresa / Lider de proyecto
	# Permisos 3 = Administrador de sucursal
	# Permisos 4 = Trabajador
	def __str__(self):
		return str(self.nombre)

class Rol(models.Model):
	id_r = models.AutoField(primary_key = True, null=False, blank = False)
	nombre = models.CharField(max_length=144)
	# Rol 1 = Directivo
	# Rol 2 = Operativo
	# Rol 3 = Ambos

	def __str__(self):
		return str(self.nombre)

class Cargo(models.Model):
	id_car = models.AutoField(primary_key = True, null=False, blank = False)
	nombre = models.CharField(max_length=144)
	funciones = models.TextField(null = True)

	def __str__(self):
		return str(self.nombre)

class User(AbstractUser):
	rut = models.CharField( max_length=12, null=True, blank=True)
	autorizacion = models.ForeignKey(Autorizacion, null=True, on_delete=models.CASCADE)
	rol = models.ForeignKey(Rol, null=True, on_delete=models.CASCADE)
	sucursal = models.ForeignKey(Sucursal, null=True, on_delete=models.CASCADE)
	empresa = models.ForeignKey(Empresa, null=True, on_delete=models.CASCADE)
	cargo = models.ForeignKey(Cargo, null=True, on_delete=models.CASCADE)
	ultima_cambio_contraseña = models.DateTimeField(blank = True, auto_now_add=False, null=True)

	class Meta:
		db_table = 'auth_user'
	def __str__(self):
		return str(self.first_name+" "+self.last_name)

class Logs(models.Model):
	usuario = models.ForeignKey(User, null=True, on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return str(self.usuario)

class UsuarioGuardado(models.Model):
	nombre = models.CharField( max_length=254)
	apellidos = models.CharField( max_length=254)
	email = models.EmailField( max_length=254)
	rut = models.CharField( max_length=12)
	rol = models.ForeignKey(Rol, null=True, on_delete=models.CASCADE)
	cargo = models.ForeignKey(Cargo, null=True, on_delete=models.CASCADE)
	sucursal = models.ForeignKey(Sucursal, null=True, on_delete=models.CASCADE)
	empresa = models.ForeignKey(Empresa, null=True, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.nombre)
