from django import forms
from .models import User, Empresa, Sucursal, UsuarioGuardado, Cargo
from django.contrib.auth.forms import UserCreationForm


class UsuarioGuardadoForm(forms.ModelForm):
	class Meta:
		model = UsuarioGuardado
		fields = ('rut','nombre','apellidos', 'email', 'rol', 'cargo')
		widgets = {
			'rut': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'apellidos': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'email': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class UsuarioForm(forms.ModelForm):
	class Meta:
		model = UsuarioGuardado
		fields = ('rut','nombre','apellidos', 'rol','sucursal', 'email')
		widgets = {
			'rut': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'apellidos': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'email': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}
	def __init__(self, *args, **kwargs):
		user = kwargs.pop('user', None)
		super().__init__(*args, **kwargs)
		self.fields['sucursal'].queryset =  Sucursal.objects.filter(empresa = user.empresa)

class UsuarioSupervisorForm(forms.ModelForm):
	class Meta:
		model = UsuarioGuardado
		fields = ('rut','nombre','apellidos', 'rol', 'email', 'empresa')
		widgets = {
			'rut': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'apellidos': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'email': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class UsuarioGeneralForm(forms.ModelForm):
	class Meta:
		model = UsuarioGuardado
		fields = ('rut','nombre','apellidos', 'email')
		widgets = {
			'rut': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'apellidos': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'email': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}


class EmpresaForm(forms.ModelForm):
	class Meta:
		model = Empresa
		fields = ('nombre','giro','rut','direccion','telefono','email')
		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'giro': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'rut': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'direccion': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'telefono': forms.NumberInput(
				attrs = {
					'class':'form-control'
				}
			),
			'email': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class SucursalForm(forms.ModelForm):
	class Meta:
		model = Sucursal
		fields = ('nombre','empresa')
		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class SucursalAdminForm(forms.ModelForm):
	class Meta:
		model = Sucursal
		fields = ('nombre',)
		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class CargoForm(forms.ModelForm):
	class Meta:
		model = Cargo
		fields = ('nombre','funciones')
		widgets = {
			'nombre': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'funciones': forms.Textarea(
				attrs = {
					'class':'form-control'
				}
			)
		}

class EditarPerfilForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ('first_name','last_name', 'email')
		widgets = {
			'first_name': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'last_name': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'email': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}

class CambiarContraseñaForm(UserCreationForm):
	class Meta:
		model = User
		fields = ('password1','password2',)
		widgets = {
			'password1': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			),
			'password2': forms.TextInput(
				attrs = {
					'class':'form-control'
				}
			)
		}