from django.shortcuts import render, redirect
from .forms import CambiarContraseñaForm, EditarPerfilForm, UsuarioSupervisorForm,  EmpresaForm, SucursalForm, SucursalAdminForm, UsuarioGuardadoForm, UsuarioForm, UsuarioGeneralForm, CargoForm
from .models import Autorizacion, Sucursal, User, Empresa, Logs
from preguntas.models import Respuesta, DiasEncuestaEmpresa
from reportes.models import Cumplimiento, RecomendacionesLogs
from tablib import Dataset
from .resources import UsuarioResource
import datetime

#Contraseña random
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password

# Email
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.conf import settings

# Funcion para obtener el id del usuario
def idusuario(request):	
	return request.user.id

def send_email(usuario, contraseña):
	template_name = get_template('correo.html')
	user = User.objects.get(id = usuario)
	data = {
	'usuario' : user,
	'contraseña' : contraseña,
	'email' : user.email
	}
	content = template_name.render(data)
	email = EmailMultiAlternatives(
		'Nuevo usuario de PDS',
		'Correo automático de PDS',
		settings.EMAIL_HOST_USER,
		[user.email]
		)
	email.attach_alternative(content, 'text/html')
	email.send()

#Agregar Supervisor Sucursal
def agregar_usuario(request):
	template_name = 'agregar_usuarios.html'
	form = UsuarioForm(user=request.user)
	if request.method == 'POST':
		form = UsuarioForm(request.POST, user=request.user)
		if form.is_valid():
			contraseña = BaseUserManager().make_random_password()
			usuario = form.save(commit=False)
			guardado = User.objects.create_user(
				username = usuario.rut,
				email = usuario.email,
				password = contraseña,
				first_name = usuario.nombre,
				last_name = usuario.apellidos,
				rol = usuario.rol,
				cargo = usuario.cargo,
				autorizacion = Autorizacion.objects.get(id_a = 3),
				sucursal  = usuario.sucursal,
				rut = usuario.rut
				)
			guardado.save()
			send_email(guardado.id, contraseña)
			return redirect('agregar_usuario')
	else:
		form = UsuarioForm(user=request.user)

	data = {
		'form':form
	}
	return render(request, template_name, data)

#Agregar Supervidor Empresa
def agregar_supervisor(request):
	template_name = 'agregar_usuarios_supervisor.html'
	form = UsuarioSupervisorForm()
	if request.method == 'POST':
		form = UsuarioSupervisorForm(request.POST)
		if form.is_valid():
			contraseña = BaseUserManager().make_random_password()
			usuario = form.save(commit=False)
			guardado = User.objects.create_user(
				username = usuario.rut,
				email = usuario.email,
				password = contraseña,
				first_name = usuario.nombre,
				last_name = usuario.apellidos,
				rol = usuario.rol,
				autorizacion = Autorizacion.objects.get(id_a = 2),
				empresa  = usuario.empresa,
				rut = usuario.rut
				)
			guardado.save()
			send_email(guardado.id, contraseña)
			return redirect('agregar_supervisor')
	else:
		form = UsuarioSupervisorForm()

	data = {
		'form':form
	}
	return render(request, template_name, data)

#Agregar Trabajador
def agregar_trabajador(request):
	template_name = 'agregar_trabajador.html'
	form = UsuarioGuardadoForm()
	userid=idusuario(request)
	sucur = User.objects.get(id = userid)
	if request.method == 'POST':
		form = UsuarioGuardadoForm(request.POST)
		if form.is_valid():
			contraseña = BaseUserManager().make_random_password()
			usuario = form.save(commit=False)
			guardado = User.objects.create_user(
				username = usuario.rut,
				email = usuario.email,
				password = contraseña,
				first_name = usuario.nombre,
				last_name = usuario.apellidos,
				rol = usuario.rol,
				cargo = usuario.cargo,
				autorizacion = Autorizacion.objects.get(id_a = 4),
				sucursal  = Sucursal.objects.get(id_su = sucur.sucursal.id_su),
				rut = usuario.rut
				)
			guardado.save()
			send_email(guardado.id, contraseña)
			return redirect('agregar_trabajador')
	else:
		form = UsuarioGuardadoForm()

	data = {
		'form':form
	}
	return render(request, template_name, data)

#Agregar Supervisor
def agregar_supervisor_general(request):
	template_name = 'agregar_supervisor_general.html'
	form = UsuarioGeneralForm()
	userid=idusuario(request)
	sucur = User.objects.get(id = userid)
	if request.method == 'POST':
		form = UsuarioGeneralForm(request.POST)
		if form.is_valid():
			contraseña = BaseUserManager().make_random_password()
			usuario = form.save(commit=False)
			guardado = User.objects.create_user(
				username = usuario.rut,
				email = usuario.email,
				password = contraseña,
				first_name = usuario.nombre,
				last_name = usuario.apellidos,
				autorizacion = Autorizacion.objects.get(id_a = 1),
				rut = usuario.rut
				)
			guardado.save()
			send_email(guardado.id, contraseña)
			return redirect('agregar_supervisor_general')
	else:
		form = UsuarioGeneralForm()

	data = {
		'form':form
	}
	return render(request, template_name, data)

def agregar_empresa(request):
	template_name = 'agregar_empresa.html'
	form = EmpresaForm()
	if request.method == 'POST':
		form = EmpresaForm(request.POST)
		if form.is_valid():
			datos = form.save(commit = False)
			datos.save()
			fecha_inicial = datetime.datetime.utcnow()
			fecha_final = fecha_inicial +datetime.timedelta(days=16)
			dias = DiasEncuestaEmpresa(
				empresa = datos,
				fecha_inicio = fecha_inicial,
				fecha_fin = fecha_final)
			dias.save()
			return redirect('agregar_empresa')
	else:
		form = EmpresaForm()

	data = {
		'form':form
	}
	return render(request, template_name, data)

def agregar_sucursal(request):
	template_name = 'agregar_sucursal.html'
	userid=idusuario(request)
	permisos = User.objects.get(id = userid)
	if permisos.autorizacion.id_a == 2:
		form = SucursalAdminForm()
		if request.method == 'POST':
			form = SucursalAdminForm(request.POST)
			if form.is_valid():
				sucur = form.save(commit=False)
				sucur.empresa = Empresa.objects.get(id_e = permisos.empresa.id_e)
				sucur.save()
				return redirect('agregar_sucursal')
		else:
			form = SucursalForm()
	else:
		form = SucursalForm()
		if request.method == 'POST':
			form = SucursalForm(request.POST)
			if form.is_valid():
				form.save()
				return redirect('agregar_sucursal')
		else:
			form = SucursalForm()

	data = {
		'form':form
	}
	return render(request, template_name, data)

def logs(request):
	template_name = 'logs.html'
	userid=idusuario(request)
	usuario_jefe = User.objects.get(id = userid)
	usuarios = Logs.objects.filter(
		usuario__sucursal__empresa = usuario_jefe.empresa
		).order_by('usuario')
	actual = Logs.objects.filter(
		usuario__sucursal__empresa = usuario_jefe.empresa
		).order_by('usuario').first()
	ingresos = 0
	cantidad = []

	for x in usuarios:
		if actual.usuario != x.usuario:
			respuestas = Respuesta.objects.filter(
				usuario_dueño = actual.usuario
				).count()
			respuestas = (respuestas/212)*100
			recomendaciones = Cumplimiento.objects.filter(
				usuario_delegado = actual.usuario
				).count()
			recom_realizadas = Cumplimiento.objects.filter(
				usuario_delegado = actual.usuario,
				cumplimiento = True
				).count()
			recom_faltantes = Cumplimiento.objects.filter(
				usuario_delegado = actual.usuario,
				cumplimiento = False
				).count()
			cantidad.append([actual, ingresos, respuestas, recomendaciones, recom_realizadas, recom_faltantes, actual.usuario.id])
			actual = x
			ingresos = 1
		else:
			ingresos = ingresos +1
	actual = x
	respuestas = Respuesta.objects.filter(
				usuario_dueño = actual.usuario
				).count()
	respuestas = (respuestas/212)*100
	recomendaciones = Cumplimiento.objects.filter(
				usuario_delegado = actual.usuario
				).count()
	recom_realizadas = Cumplimiento.objects.filter(
				usuario_delegado = actual.usuario,
				cumplimiento = True
				).count()
	recom_faltantes = Cumplimiento.objects.filter(
				usuario_delegado = actual.usuario,
				cumplimiento = False
				).count()
	cantidad.append([actual, ingresos, respuestas, recomendaciones, recom_realizadas, recom_faltantes, actual.usuario.id])

	data = {
		'logs':cantidad
	}
	return render(request, template_name, data)

def agregar_cargo(request):
	template_name = 'agregar_cargo.html'
	form = CargoForm()
	if request.method == 'POST':
		form = CargoForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('agregar_cargo')
	else:
		form = CargoForm()

	data = {
		'form':form
	}
	return render(request, template_name, data)

def editar_perfil(request):
	template_name = 'editar_perfil.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	form = EditarPerfilForm(instance = usuario)
	if request.method == 'POST':
		form = EditarPerfilForm(request.POST, instance = usuario)
		if form.is_valid():
			form.save()
			return redirect('editar_perfil')
	else:
		form = EditarPerfilForm(instance = usuario)

	data = {
		'form':form
	}
	return render(request, template_name, data)

def cambiar_contraseña(request):
	template_name = 'cambiar_contraseña.html'
	userid=idusuario(request)
	usuario = User.objects.get(id = userid)
	form = CambiarContraseñaForm(instance = usuario)
	if request.method == 'POST':
		form = CambiarContraseñaForm(request.POST, instance = usuario)
		if form.is_valid():
			form.save()
			return redirect('cambiar_contraseña')
	else:
		form = CambiarContraseñaForm(instance = usuario)

	data = {
		'form':form
	}
	return render(request, template_name, data)

def log_recomendacion(request):
	template_name = 'log_recomendacion.html'
	userid = request.GET['usuario']
	usuario = User.objects.get(id = userid)
	# data = RecomendacionesLogs.objects.filter(cumplimiento__usuario_delegado = usuario)
	# print(data)
	# for x in data:
	# 	print(x)
	data = {
	'datos' : RecomendacionesLogs.objects.filter(cumplimiento__usuario_delegado = usuario)
	}
	return render(request, template_name, data)