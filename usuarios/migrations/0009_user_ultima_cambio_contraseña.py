# Generated by Django 2.2.1 on 2020-08-06 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0008_usuarioguardado_rut'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='ultima_cambio_contraseña',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
