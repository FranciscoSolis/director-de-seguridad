# Generated by Django 2.2.1 on 2020-07-12 21:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0002_remove_user_empresa'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='empresa',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='usuarios.Empresa'),
        ),
    ]
