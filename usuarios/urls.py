from django.urls import path
from usuarios import views
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', login_required(views.agregar_usuario), name="agregar_usuario"),
    path('agregar_supervisor', login_required(views.agregar_supervisor), name="agregar_supervisor"),
    path('agregar_trabajador', login_required(views.agregar_trabajador), name="agregar_trabajador"),
    path('agregar_supervisor_general', login_required(views.agregar_supervisor_general), name="agregar_supervisor_general"),
    path('agregar_empresa', login_required(views.agregar_empresa), name="agregar_empresa"),
    path('agregar_sucursal', login_required(views.agregar_sucursal), name="agregar_sucursal"),
    path('agregar_cargo', login_required(views.agregar_cargo), name="agregar_cargo"),
    path('logs', login_required(views.logs), name="logs"),
    path('editar_perfil', login_required(views.editar_perfil), name="editar_perfil"),
    path('cambiar_contraseña', login_required(views.cambiar_contraseña), name="cambiar_contraseña"),
    path('logs/log_recomendacion', login_required(views.log_recomendacion), name="log_recomendacion")
]
