from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from import_export.admin import ImportExportModelAdmin
from .models import *

class EmpresaAdmin(ImportExportModelAdmin):
	pass

class UserAdmin(ImportExportModelAdmin):
	pass

admin.site.register(User, UserAdmin)
admin.site.register(Empresa, EmpresaAdmin)
admin.site.register(Sucursal)
admin.site.register(Autorizacion)
admin.site.register(Rol)
admin.site.register(Cargo)
admin.site.register(Logs)