from import_export import resources  
from .models import User  


class UsuarioResource(resources.ModelResource): 
	class Meta:  
		model = User
		fields = ('username','first_name','last_name','email','rol','cargo')
		export_order = ('username','first_name','last_name','email','rol','cargo')