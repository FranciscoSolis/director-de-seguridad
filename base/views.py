from django.shortcuts import render

def index(request):
	template_name = 'login.html'
	data = {}
	return render(request, template_name, data)

def base(request):
	template_name = 'base.html'
	data = {
	}
	return render(request, template_name, data)
